
import serial
import sys
import time
from functools import wraps
import errno
import os
import signal
import io





class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator


# Device to connect to comm port 


def start_serial_comms(comm_port):
	""" 
    Attempts to connect this machine to it's comm_port.
    :param comm_port: The name of the comm port
    :param baud_rate: The speed to open the serial connection at

    :return:
    """
	try:
		dev = serial.Serial(comm_port, baudrate=115200,
			 bytesize= serial.EIGHTBITS,parity = serial.PARITY_NONE,
			 stopbits=serial.STOPBITS_ONE,rtscts=0 )

		print ("Connected successfully to %s." % (comm_port))
		return dev
	except Exception as e:
		print("Exception in connecting to the port %s: %s" % (comm_port, e))
		#return False
		exit()

def read_data (dev):
	return dev.readline() # read 20 byte of data 

#@timeout(60, os.strerror(errno.ETIMEDOUT))
def write_data (data,dev):
	input =''
	dev.write(data.encode('utf-8'))
	while ((data != input)):
		#print("writing data")
		dev.write(data.encode('utf-8'))
		dev.flush()

		input = read_data(dev).strip()
		#print(input)
		input = input.decode('ascii', errors='ignore').strip()
		print("|{0},{1}|".format(input,data.strip()))
		time.sleep(2) 
		if input.strip() == data.strip():
			print("Input matched!")
			dev.close()
			break

		
		if (input and (data != input)):
			dev.write(data.encode('utf-8'))
	dev.close() 


def main():
	
	print("Please enter your commands below.\r\nOptions include DR_$$ for datarate, TX_$$ for transmission power, read to read line or exit to leave")
	dev=start_serial_comms("/dev/cu.usbmodem141423")
	time.sleep(1)
	#dev.dtr(0)
	time.sleep(1)
	#while 1:
	sio = io.TextIOWrapper(io.BufferedRWPair(dev, dev))

	data_input ='^dr02$\r\n'
	#sio.write(str(data_input.encode('utf-8')))
		#write_data(data_input,dev)
	dev.write(b'^dr01$')
		#dev.write(b'^dr02$\r\n')
	dev.flush()
	print(read_data(dev))
	#print(read_data(dev))
	#print(read_data(dev))
	#print(read_data(dev))
	#print(read_data(dev))
	#print(read_data(dev))
	#print(read_data(dev))
	dev.close()


main()			
