#run example controller
#run with -v for debugging
./controller --config ./controller_config.yaml 

#run example agent
#run with -v for debugging
./agent --config ./agent_config.yaml